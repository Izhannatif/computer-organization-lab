org 100h
.stack 100h
.model small

.data 

    input db "enter char: $"
    lower db "lowercase: $"
    upper db "uppercase: $"
    char db ?
 
.code 
  
  main proc
       
    mov ax,@data   
    mov ds,ax      
 
    mov dx,offset input    
    mov ah,09      
    int 21h
                 
    mov ah,01      
    int 21h
 
    mov char,al  
                           
    mov dl,10
    mov ah,2
    int 21h
    mov dl,13
    mov ah,2
    int 21h
 
    mov dx,offset lower    
    mov ah,09      
    int 21h
 
    mov dl,char  
                 
    mov ah,02     
    int 21h     
 
    mov dl,10
    mov ah,2
    int 21h
    mov dl,13
    mov ah,2
    int 21h
 
    mov dx,offset upper  
    mov ah,09      
    int 21h
    
    sub char,32  
    mov dl,char  
                    
    mov ah,02      
    int 21h
 
main endp
end main