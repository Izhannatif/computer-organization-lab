org 100h
.stack 100h
.model small
.data
    arr db 1,2,6,4,9
    result1 db ?
    result2 db ?
    result3 db ?
    
.code

main proc
    mov ax,@data
    mov ds,ax
    
    mov cx,1
    mov si,0 
    
    addLoop:
    mov bl,arr[si] 
    inc si
    add bl,arr[si]
    mov dl,bl
    add dl,30h
    mov ah,2
    mov result1,bl
    int 21h 
    
   loop addLoop
   
   
    mov cx,1
    inc si
    mov ah,2
    
    subLoop:
    mov bl,arr[si] 
    
    inc si
    
    sub bl,arr[si]
    mov dl,bl
    add dl,48
    mov result2,bl
    int 21h
    
    loop subLoop
   
   
   mov bl,result1
   add bl,result2
   mov result3,bl
   mov dl,bl
   add dl,30h
   mov ah,2
   int 21h
   
   mov bl,arr[4]
   sub bl,result3
   mov dl,bl
   add dl,30h
   mov ah,2
   int 21h
    
    
    
    main endp
end main
ret
