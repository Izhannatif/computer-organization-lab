.model small
.stack 100h

.data
          multiply db ?
.code
main proc
    
    mov ah,1
    int 21h
    sub al,48
    mov bl,al
    
    mov ah,01
    int 21h
    sub al,48 
    
    mul bl
    
    mov dl,al
    add dl,48
    mov ah,2
    int 21h    

    main endp
end main
ret