org 100h
.stack 100h
.model small
.data
    arr db 1,2,6,4,5
.code

main proc
    mov ax,@data
    mov ds,ax
    mov cx,1
    mov si,0
    output1:
    mov bl,arr[si] 
    inc si
    add bl,arr[si]
    mov dl,bl
    add dl,30h
    mov ah,2
    int 21h
   loop output1
   
    mov cx,1
    inc si
    mov ah,2
    
    output2:
    mov bl,arr[si] 
    
    inc si
    
    sub bl,arr[si]
    mov dl,bl
    add dl,48
    int 21h
   
   loop output2

    
    
    
    main endp
end main
ret
