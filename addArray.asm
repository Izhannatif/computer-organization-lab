org 100h
.stack 100h
.model small
.data
    arr db 1,2,3,4,5
.code

main proc
    mov ax,@data
    mov ds,ax
    
    mov si,0
    
    mov bl,arr[si] 
    inc si
    add bl,arr[si]
    
    mov dl,bl
    add dl,30h
    
    mov ah,2
    int 21h
    
    main endp
end main
ret
